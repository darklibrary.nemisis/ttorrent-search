package hu.tagsoft.ttorrent.search;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class TransdroidCursorAdapter extends CursorAdapter {

    public TransdroidCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        TransdroidCursorWrapper wrapper = new TransdroidCursorWrapper(cursor);

        viewHolder.name.setText(wrapper.getName());
        viewHolder.size.setText("Size: " + FormatUtil.formatBytes(wrapper.getSize()));
        viewHolder.peers.setText("Seeders: " + wrapper.getSeeders() + "   Leechers: "
                + wrapper.getLeechers());
        viewHolder.date.setText("Added: " + wrapper.getAdded());
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.search_list_item, parent, false);
        v.setTag(new ViewHolder(v));
        bindView(v, context, cursor);
        return v;
    }

    static class ViewHolder {
        TextView name;
        TextView size;
        TextView peers;
        TextView date;

        public ViewHolder(View view) {
            name = view.findViewById(R.id.search_list_item_name);
            size = view.findViewById(R.id.search_list_item_size);
            peers = view.findViewById(R.id.search_list_item_peers);
            date = view.findViewById(R.id.search_list_item_date);
        }
    }

}
