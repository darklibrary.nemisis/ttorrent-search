package hu.tagsoft.ttorrent.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AlertDialog;

class TransdroidSearchInstaller {

    private static final String url = "http://www.transdroid.org/latest-search";

    static boolean checkInstalledTransdroidVersion(Context context) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            int latestVersion = sharedPreferences.getInt(
                    "TRANSDROID_SEARCH_VERSION",
                    0);

            return context.getPackageManager().getPackageInfo(
                    "org.transdroid.search", 0).versionCode >= latestVersion;
        } catch (NameNotFoundException ex) {
            return false;
        }
    }

    static void showTransdroidSearchNotInstalled(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(
                        activity.getString(R.string.dialog_transdroid_not_installed_title))
                .setMessage(
                        activity.getString(R.string.dialog_transdroid_not_installed))
                .setPositiveButton(R.string.dialog_button_download,
                        (dialog, which) -> {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            activity.startActivity(i);
                            activity.finish();
                        }
                )
                .setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> activity.finish())
                .show();
    }

}
