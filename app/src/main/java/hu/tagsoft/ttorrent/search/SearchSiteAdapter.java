package hu.tagsoft.ttorrent.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SearchSiteAdapter extends ArrayAdapter<SearchSite> {

    private Context context;

    public SearchSiteAdapter(Context context) {
        super(context, 0);
        this.context = context;
    }

    public int getSitePosition(String key) {
        for (int i = 0; i < getCount(); i++) {
            if (getItem(i).getKey().equals(key))
                return i;
        }

        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.support_simple_spinner_dropdown_item, parent, false);

        TextView textView = convertView
                .findViewById(android.R.id.text1);

        textView.setText(getItem(position).getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.support_simple_spinner_dropdown_item, parent, false);

        TextView textView = convertView
                .findViewById(android.R.id.text1);
        textView.setText(getItem(position).getName());

        return textView;
    }
}
