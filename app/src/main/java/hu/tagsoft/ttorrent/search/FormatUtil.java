package hu.tagsoft.ttorrent.search;

public class FormatUtil {
    public static String formatBytes(long l) {
        if (l < 1024) {
            return String.format("%d B", l);
        } else if (l < 1024 * 1024) {
            float speed = (float) l / 1024;
            return String.format("%.1f kB", speed);
        } else if (l < 1024 * 1024 * 1024) {
            float speed = (float) l / (1024 * 1024);
            return String.format("%.1f MB", speed);
        } else {
            float speed = (float) l / (1024 * 1024 * 1024);
            return String.format("%.1f GB", speed);
        }
    }
}
