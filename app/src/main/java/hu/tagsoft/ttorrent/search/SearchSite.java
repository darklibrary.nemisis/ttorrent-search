package hu.tagsoft.ttorrent.search;

public class SearchSite {

    private int id;
    private String key;
    private String name;
    private boolean isPrivate;

    public SearchSite(int id, String key, String name, boolean isPrivate) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public boolean isPrivate() {
        return isPrivate;
    }
}